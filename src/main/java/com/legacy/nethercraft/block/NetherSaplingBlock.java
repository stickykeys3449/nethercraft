package com.legacy.nethercraft.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.SaplingBlock;
import net.minecraft.block.trees.Tree;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class NetherSaplingBlock extends SaplingBlock
{

	public NetherSaplingBlock(Tree treeIn, Properties properties)
	{
		super(treeIn, properties);
	}

	public void tick(BlockState state, World worldIn, BlockPos pos, Random random)
	{
		super.tick(state, worldIn, pos, random);
		if (!worldIn.isAreaLoaded(pos, 1))
			return;
		if (random.nextInt(7) == 0)
		{
			this.grow(worldIn, pos, state, random);
		}
	}

	protected boolean isValidGround(BlockState state, IBlockReader worldIn, BlockPos pos)
	{
		Block block = state.getBlock();
		return block == Blocks.NETHERRACK || block == NetherBlocks.nether_dirt || super.isValidGround(state, worldIn, pos);
	}
}
