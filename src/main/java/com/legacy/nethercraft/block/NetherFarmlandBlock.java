package com.legacy.nethercraft.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FarmlandBlock;
import net.minecraft.entity.Entity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;

public class NetherFarmlandBlock extends FarmlandBlock
{

	public NetherFarmlandBlock(Block.Properties builder)
	{
		super(builder);
	}

	@Override
	public void onFallenUpon(World worldIn, BlockPos pos, Entity entityIn, float fallDistance)
	{
		if (!worldIn.isRemote && net.minecraftforge.common.ForgeHooks.onFarmlandTrample(worldIn, pos, NetherBlocks.nether_dirt.getDefaultState(), fallDistance, entityIn))
		{
			turnToDirt(worldIn.getBlockState(pos), worldIn, pos);
		}
		entityIn.fall(fallDistance, 1.0F);
	}

	public void tick(BlockState state, World worldIn, BlockPos pos, Random random)
	{
		if (!state.isValidPosition(worldIn, pos))
		{
			turnToDirt(state, worldIn, pos);
		}
		else
		{
			int i = state.get(MOISTURE);
			if (!hasWater(worldIn, pos) && !worldIn.isRainingAt(pos.up()))
			{
				if (i > 0)
				{
					worldIn.setBlockState(pos, state.with(MOISTURE, Integer.valueOf(i - 1)), 2);
				}
				else if (!hasCrops(worldIn, pos))
				{
					turnToDirt(state, worldIn, pos);
				}
			}
			else if (i < 7)
			{
				worldIn.setBlockState(pos, state.with(MOISTURE, Integer.valueOf(7)), 2);
			}
		}
	}

	private static boolean hasWater(IWorldReader worldIn, BlockPos pos)
	{
		if (worldIn.getLight(pos.up()) <= 0)
		{
			return true;
		}

		return net.minecraftforge.common.FarmlandWaterManager.hasBlockWaterTicket(worldIn, pos);
	}

	private boolean hasCrops(IBlockReader worldIn, BlockPos pos)
	{
		BlockState state = worldIn.getBlockState(pos.up());
		return state.getBlock() instanceof net.minecraftforge.common.IPlantable && canSustainPlant(state, worldIn, pos, Direction.UP, (net.minecraftforge.common.IPlantable) state.getBlock());
	}

	public static void turnToDirt(BlockState state, World worldIn, BlockPos pos)
	{
		worldIn.setBlockState(pos, nudgeEntitiesWithNewState(state, NetherBlocks.nether_dirt.getDefaultState(), worldIn, pos));
	}

	@Override
	public boolean isFertile(BlockState state, IBlockReader world, BlockPos pos)
	{
		if (world.getBlockState(pos).getBlock() == this)
		{
			return state.get(FarmlandBlock.MOISTURE) > 0;
		}
		return false;
	}
}
