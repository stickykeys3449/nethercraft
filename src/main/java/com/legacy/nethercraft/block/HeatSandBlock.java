package com.legacy.nethercraft.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FallingBlock;
import net.minecraft.block.material.Material;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.PlantType;

public class HeatSandBlock extends FallingBlock
{

	public HeatSandBlock(Block.Properties properties)
	{
		super(properties);
	}

	@Override
	public boolean canSustainPlant(BlockState state, IBlockReader world, BlockPos pos, Direction facing, IPlantable plantable)
	{
		PlantType plantType = plantable.getPlantType(world, pos.offset(facing));
		switch (plantType)
		{
		case Beach:
			boolean hasWater = (world.getBlockState(pos.east()).getMaterial() == Material.LAVA || world.getBlockState(pos.west()).getMaterial() == Material.LAVA || world.getBlockState(pos.north()).getMaterial() == Material.LAVA || world.getBlockState(pos.south()).getMaterial() == Material.LAVA);
			return hasWater;
		default:
			break;
		}
		return super.canSustainPlant(state, world, pos, facing, plantable);
	}
}