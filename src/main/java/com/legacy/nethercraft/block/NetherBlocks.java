package com.legacy.nethercraft.block;

import java.util.ArrayList;

import com.google.common.collect.Lists;
import com.legacy.nethercraft.NethercraftRegistry;
import com.legacy.nethercraft.item.util.GlowoodTree;
import com.legacy.nethercraft.item.util.NetherItemGroup;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.DoorBlock;
import net.minecraft.block.FenceBlock;
import net.minecraft.block.LadderBlock;
import net.minecraft.block.LeavesBlock;
import net.minecraft.block.LogBlock;
import net.minecraft.block.MushroomBlock;
import net.minecraft.block.PressurePlateBlock;
import net.minecraft.block.PressurePlateBlock.Sensitivity;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.StainedGlassBlock;
import net.minecraft.block.StainedGlassPaneBlock;
import net.minecraft.block.StairsBlock;
import net.minecraft.block.SugarCaneBlock;
import net.minecraft.block.TorchBlock;
import net.minecraft.block.WallTorchBlock;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.DyeColor;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class NetherBlocks
{

	public static Block nether_dirt, nether_farmland, heat_sand, lava_reeds, glowood_bookshelf;

	public static Block glowood_chest, glowood_crafting_table, netherrack_furnace, glowood_door;

	public static Block glowood_log, glowood_leaves, stripped_glowood_log, glowood_planks, glowood_sapling, glowood_fence, glowood_ladder, glowood_stairs, glowood_slab, glowood_pressure_plate;

	public static Block w_ore, foulite_ore, neridium_ore, pyridium_ore, linium_ore;

	public static Block foulite_torch, charcoal_torch, foulite_wall_torch, charcoal_wall_torch;

	public static Block neridium_block, pyridium_block, linium_block;

	public static Block slow_glass, heat_glass, slow_glass_pane, heat_glass_pane;

	public static Block green_glowshroom, purple_glowshroom;

	public static Block ghast_bomb;

	public static Block dark_wheat;

	public static Block smooth_netherrack, smooth_netherrack_slab, netherrack_stairs;

	private static IForgeRegistry<Block> iBlockRegistry;

	//public static Map<Block, ItemGroup> blockItemMap = new HashMap<>();
	//public static Map<Block, Item.Properties> blockItemPropertiesMap = new HashMap<>();

	public static ArrayList<Block> blockList = Lists.newArrayList();

	public static void initialization(Register<Block> event)
	{	
		NetherBlocks.iBlockRegistry = event.getRegistry();

		if (iBlockRegistry == null)
		{
			return;
		}

		nether_dirt = register("nether_dirt", new NetherDirtBlock(Block.Properties.from(Blocks.COARSE_DIRT).hardnessAndResistance(0.5F, 2.0F)));
		nether_farmland = register("nether_farmland", new NetherFarmlandBlock(Block.Properties.from(Blocks.FARMLAND)) {});
		heat_sand = register("heat_sand", new HeatSandBlock(Block.Properties.from(Blocks.SAND).lightValue(9)));
		glowood_log = register("glowood_log", new LogBlock(MaterialColor.GRAY, Block.Properties.from(Blocks.OAK_LOG).hardnessAndResistance(2.0F, 1000.0F).lightValue(6)));
		stripped_glowood_log = register("stripped_glowood_log", new LogBlock(MaterialColor.GRAY, Block.Properties.from(Blocks.STRIPPED_OAK_LOG).hardnessAndResistance(2.0F, 1000.0F).lightValue(6)));
		glowood_leaves = register("glowood_leaves", new LeavesBlock(Block.Properties.from(Blocks.OAK_LEAVES).hardnessAndResistance(0.2F, 400.0F).lightValue(13)));
		glowood_sapling = register("glowood_sapling", new NetherSaplingBlock(new GlowoodTree(), Block.Properties.from(Blocks.OAK_SAPLING).lightValue(4)));
		glowood_planks = register("glowood_planks", new Block(Block.Properties.from(Blocks.OAK_PLANKS).lightValue(8)));
		glowood_bookshelf = register("glowood_bookshelf", new NetherBookshelfBlock(Block.Properties.from(Blocks.BOOKSHELF).lightValue(8)));
		foulite_ore = register("foulite_ore", new Block(Block.Properties.from(Blocks.COAL_ORE).harvestLevel(0).harvestTool(ToolType.PICKAXE)));
		neridium_ore = register("neridium_ore", new Block(Block.Properties.from(Blocks.IRON_ORE).harvestLevel(1).harvestTool(ToolType.PICKAXE)));
		pyridium_ore = register("pyridium_ore", new Block(Block.Properties.from(Blocks.DIAMOND_ORE).harvestLevel(2).harvestTool(ToolType.PICKAXE)));
		linium_ore = register("linium_ore", new Block(Block.Properties.from(Blocks.DIAMOND_ORE).harvestLevel(3).harvestTool(ToolType.PICKAXE)));
		w_ore = register("w_ore", new Block(Block.Properties.from(Blocks.DIAMOND_ORE).harvestLevel(3).harvestTool(ToolType.PICKAXE)));
		neridium_block = register("neridium_block", new Block(Block.Properties.from(Blocks.IRON_BLOCK).harvestTool(ToolType.PICKAXE)));
		pyridium_block = register("pyridium_block", new Block(Block.Properties.from(Blocks.DIAMOND_BLOCK).harvestTool(ToolType.PICKAXE)));
		linium_block = register("linium_block", new Block(Block.Properties.from(Blocks.DIAMOND_BLOCK).lightValue(15).harvestTool(ToolType.PICKAXE)));
		slow_glass = register("slow_glass", new StainedGlassBlock(DyeColor.BROWN, Block.Properties.from(Blocks.GLASS)));
		heat_glass = register("heat_glass", new StainedGlassBlock(DyeColor.ORANGE, Block.Properties.from(Blocks.GLASS).lightValue(9)));
		slow_glass_pane = register("slow_glass_pane", new StainedGlassPaneBlock(DyeColor.BROWN, Block.Properties.from(Blocks.GLASS_PANE)));
		heat_glass_pane = register("heat_glass_pane", new StainedGlassPaneBlock(DyeColor.ORANGE, Block.Properties.from(Blocks.GLASS_PANE).lightValue(9)));
		green_glowshroom = register("green_glowshroom", new MushroomBlock(Block.Properties.from(Blocks.RED_MUSHROOM).lightValue(7)));
		purple_glowshroom = register("purple_glowshroom", new MushroomBlock(Block.Properties.from(Blocks.BROWN_MUSHROOM).lightValue(5)));
		glowood_ladder = register("glowood_ladder", new LadderBlock(Block.Properties.from(Blocks.LADDER).lightValue(6)) {});
		glowood_door = register("glowood_door", new DoorBlock(Block.Properties.from(Blocks.OAK_DOOR).lightValue(8)) {});
		glowood_fence = register("glowood_fence", new FenceBlock(Block.Properties.from(glowood_planks)));
		glowood_stairs = register("glowood_stairs", new StairsBlock(glowood_planks.getDefaultState(), Block.Properties.from(glowood_planks)) {});
		glowood_slab = register("glowood_slab", new SlabBlock(Block.Properties.from(glowood_planks)));
		glowood_pressure_plate = register("glowood_pressure_plate", new PressurePlateBlock(Sensitivity.EVERYTHING, Block.Properties.from(Blocks.OAK_PRESSURE_PLATE)) {});
		glowood_crafting_table = register("glowood_crafting_table", new GlowoodCraftingTableBlock(Block.Properties.from(Blocks.CRAFTING_TABLE).lightValue(8)) {});
		glowood_chest = registerBlock("glowood_chest", new NetherChestBlock(Block.Properties.from(Blocks.CHEST).lightValue(8)) {});
		netherrack_furnace = register("netherrack_furnace", new NetherrackFurnaceBlock(Block.Properties.from(Blocks.FURNACE)) {});
		ghast_bomb = register("ghast_bomb", new GhastBombBlock(Block.Properties.from(Blocks.SAND).lightValue(9)));
		foulite_torch = registerBlock("foulite_torch", new TorchBlock(Block.Properties.from(Blocks.TORCH)) {});
		charcoal_torch = registerBlock("charcoal_torch", new TorchBlock(Block.Properties.from(Blocks.TORCH)) {});
		foulite_wall_torch = registerBlock("foulite_wall_torch", new WallTorchBlock(Block.Properties.from(Blocks.WALL_TORCH)) {});
		charcoal_wall_torch = registerBlock("charcoal_wall_torch", new WallTorchBlock(Block.Properties.from(Blocks.WALL_TORCH)) {});
		lava_reeds = registerBlock("lava_reeds", new SugarCaneBlock(Block.Properties.from(Blocks.SUGAR_CANE)) {});
		dark_wheat = registerBlock("dark_wheat", new DarkWheatBlock(Block.Properties.from(Blocks.WHEAT)) {});
		smooth_netherrack = register("smooth_netherrack", new Block(Block.Properties.from(Blocks.NETHERRACK)));
		smooth_netherrack_slab = register("smooth_netherrack_slab", new SlabBlock(Block.Properties.from(smooth_netherrack)));
	}

	public static Block register(String name, Block block)
	{
		return register(name, block, NetherItemGroup.BLOCKS);
	}
	
	public static <T extends ItemGroup> Block register(String key, Block block, T itemGroup)
	{
		blockList.add(block);
		return registerBlock(key, block);
	}

	public static Block registerBlock(String key, Block block)
	{
		NethercraftRegistry.register(iBlockRegistry, key, block);
		return block;
	}
}