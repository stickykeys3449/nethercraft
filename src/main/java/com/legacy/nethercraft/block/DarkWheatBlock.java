package com.legacy.nethercraft.block;

import java.util.Random;

import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropsBlock;
import net.minecraft.block.FarmlandBlock;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;

public class DarkWheatBlock extends CropsBlock
{

	public static final IntegerProperty NETHER_CROP_AGE = BlockStateProperties.AGE_0_5;

	private static final VoxelShape[] SHAPE = new VoxelShape[]
	{ Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 2.0D, 16.0D), Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 4.0D, 16.0D), Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 6.0D, 16.0D), Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 8.0D, 16.0D), Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 10.0D, 16.0D), Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 12.0D, 16.0D), Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 14.0D, 16.0D), Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 16.0D, 16.0D) };

	public DarkWheatBlock(Block.Properties properties)
	{
		super(properties);
	}

	public IntegerProperty getAgeProperty()
	{
		return NETHER_CROP_AGE;
	}

	public int getMaxAge()
	{
		return 5;
	}

	protected IItemProvider getSeedsItem()
	{
		return NetherItems.dark_seeds;
	}

	public void tick(BlockState state, World worldIn, BlockPos pos, Random random)
	{
		if (random.nextInt(3) != 0)
		{
			if (!worldIn.isAreaLoaded(pos, 1))
			{
				return;
			}
			int i = this.getAge(state);
			if (i < this.getMaxAge())
			{
				float f = getGrowthChance(this, worldIn, pos);
				if (ForgeHooks.onCropsGrowPre(worldIn, pos, state, random.nextInt((int) (25.0F / f) + 1) == 0))
				{
					worldIn.setBlockState(pos, this.withAge(i + 1), 2);
					ForgeHooks.onCropsGrowPost(worldIn, pos, state);
				}
			}
		}
	}

	protected int getBonemealAgeIncrease(World worldIn)
	{
		return super.getBonemealAgeIncrease(worldIn) / 3;
	}

	protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder)
	{
		builder.add(NETHER_CROP_AGE);
	}

	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context)
	{
		return SHAPE[state.get(this.getAgeProperty())];
	}

	public boolean isValidPosition(BlockState state, IWorldReader worldIn, BlockPos pos)
	{
		BlockPos blockpos = pos.down();

		if (state.getBlock() == this)
		{
			return worldIn.getBlockState(blockpos).canSustainPlant(worldIn, blockpos, Direction.UP, this);
		}

		return this.isValidGround(worldIn.getBlockState(blockpos), worldIn, blockpos);
	}

	protected boolean isValidGround(BlockState state, IBlockReader worldIn, BlockPos pos)
	{
		return state.getBlock() instanceof FarmlandBlock;
	}
}