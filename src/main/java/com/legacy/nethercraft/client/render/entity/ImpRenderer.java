package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.client.render.model.ImpModel;
import com.legacy.nethercraft.entity.ImpEntity;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class ImpRenderer extends MobRenderer<ImpEntity, ImpModel<ImpEntity>>
{

    private static final ResourceLocation RED_IMP_TEXTURE = new ResourceLocation("nethercraft", "textures/entity/imp/imp_red.png");

    private static final ResourceLocation ORANGE_IMP_TEXTURE = new ResourceLocation("nethercraft", "textures/entity/imp/imp_orange.png");

    private static final ResourceLocation GREEN_IMP_TEXTURE = new ResourceLocation("nethercraft", "textures/entity/imp/imp_green.png");

	public ImpRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new ImpModel<>(), 0.5F);
	}

	@Override
    protected void preRenderCallback(ImpEntity imp, float partialTickTime)
    {
		super.preRenderCallback(imp, partialTickTime);
		
		if (imp.getImpType() == 2)
		{
			GlStateManager.scalef(1.8F, 1.8F, 1.8F);
		}
		else if (imp.getImpType() == 1)
		{
			GlStateManager.scalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GlStateManager.scalef(1.0F, 1.0F, 1.0F);
		}
    }

	@Override
	protected ResourceLocation getEntityTexture(ImpEntity entity)
	{
		return entity.getImpType() == 2 ? RED_IMP_TEXTURE : entity.getImpType() == 1 ? ORANGE_IMP_TEXTURE : GREEN_IMP_TEXTURE;
	}

}