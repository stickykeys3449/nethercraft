package com.legacy.nethercraft.client.render.model;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.MathHelper;

public class ImpModel<T extends Entity> extends EntityModel<T>
{

    public RendererModel Body;
    public RendererModel Head;
    public RendererModel Leg1;
    public RendererModel Leg2;
    public RendererModel Arm1;
    public RendererModel Arm2;

    public ImpModel()
    {
        this.Body = new RendererModel(this, 16, 16);
        this.Body.addBox(-5F, 0.0F, -18.5F, 10, 5, 9, 0.0F);
        this.Body.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Head = new RendererModel(this, 0, 0);
        this.Head.addBox(-4.0F, -8.0F, -4.0F, 10, 8, 8, 0.0F);
        this.Head.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Leg1 = new RendererModel(this, 0, 16);
        this.Leg1.addBox(-2F, 0.0F, -2F, 4, 6, 4, 0.0F);
        this.Leg1.setRotationPoint(-3F, 18F, 2.0F);
        this.Leg2 = new RendererModel(this, 0, 16);
        this.Leg2.addBox(-2F, 0.0F, -2F, 4, 6, 4, 0.0F);
        this.Leg2.setRotationPoint(3F, 18F, 2.0F);
        this.Arm1 = new RendererModel(this, 32, 0);
        this.Arm1.addBox(-1.5F, 0.0F, 1.0F, 2, 5, 2, 0.0F);
        this.Arm1.setRotationPoint(6F, 10F, 0.0F);
        this.Arm2 = new RendererModel(this, 32, 0);
        this.Arm2.addBox(-0.5F, 0.0F, 1.0F, 2, 5, 2, 0.0F);
        this.Arm2.setRotationPoint(-6F, 10F, 0.0F);
    }

    @Override
    public void render(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
    {
    	this.setRotationAngles(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
    	GlStateManager.translatef(0.0F, 0.0F, -0.2F);
        this.Body.render(scale);
        this.Head.render(scale);
        this.Leg1.render(scale);
        this.Leg2.render(scale);
        this.Arm1.render(scale);
        this.Arm2.render(scale);
    }

    @Override
    public void setRotationAngles(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor)
    {
        boolean flag = entityIn instanceof LivingEntity && ((LivingEntity)entityIn).getTicksElytraFlying() > 4;
        this.Head.rotateAngleY = netHeadYaw * 0.017453292F;

        if (flag)
        {
            this.Head.rotateAngleX = -((float)Math.PI / 4F);
        }
        else
        {
            this.Head.rotateAngleX = headPitch * 0.017453292F;
        }

        this.Head.rotationPointY = 9.5F;
        this.Head.rotationPointX = -1.0F;
        this.Head.rotationPointZ = 2.0F;

    	this.Body.rotateAngleX = 1.570796F;
    	this.Arm1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + 0.0F) * 1.4F * limbSwingAmount;
    	this.Arm2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + 3.141593F) * 1.4F * limbSwingAmount;
        this.Leg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + 0.0F) * 1.4F * limbSwingAmount;
        this.Leg2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + 3.141593F) * 1.4F * limbSwingAmount;
    }

}