package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.client.render.model.LavaBoatModel;
import com.legacy.nethercraft.entity.misc.LavaBoatEntity;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class LavaBoatRenderer extends EntityRenderer<LavaBoatEntity>
{

	protected LavaBoatModel modelBoat = new LavaBoatModel();

	public LavaBoatRenderer(EntityRendererManager renderManager)
	{
		super(renderManager);
	}

	public void doRender(LavaBoatEntity entity, double x, double y, double z, float entityYaw, float partialTicks)
	{
		GlStateManager.pushMatrix();
		this.setupTranslation(x, y, z);
		this.setupRotation(entity, entityYaw, partialTicks);
		this.bindEntityTexture(entity);
		if (this.renderOutlines)
		{
			GlStateManager.enableColorMaterial();
			GlStateManager.setupSolidRenderingTextureCombine(this.getTeamColor(entity));
		}
		this.modelBoat.render(entity, partialTicks, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
		if (this.renderOutlines)
		{
			GlStateManager.tearDownSolidRenderingTextureCombine();
			GlStateManager.disableColorMaterial();
		}
		GlStateManager.popMatrix();
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
	}

	public void setupRotation(LavaBoatEntity p_188311_1_, float p_188311_2_, float p_188311_3_)
	{
		GlStateManager.rotatef(180.0F - p_188311_2_, 0.0F, 1.0F, 0.0F);
		float f = (float) p_188311_1_.getTimeSinceHit() - p_188311_3_;
		float f1 = p_188311_1_.getDamageTaken() - p_188311_3_;
		if (f1 < 0.0F)
		{
			f1 = 0.0F;
		}
		if (f > 0.0F)
		{
			GlStateManager.rotatef(MathHelper.sin(f) * f * f1 / 10.0F * (float) p_188311_1_.getForwardDirection(), 1.0F, 0.0F, 0.0F);
		}
		GlStateManager.scalef(-1.0F, -1.0F, 1.0F);
	}

	public void setupTranslation(double p_188309_1_, double p_188309_3_, double p_188309_5_)
	{
		GlStateManager.translatef((float) p_188309_1_, (float) p_188309_3_ + 0.375F, (float) p_188309_5_);
	}

	@Override
	protected ResourceLocation getEntityTexture(LavaBoatEntity entity)
	{
		return Nethercraft.locate("textures/entity/boat/boat_glow_wood.png");
	}

	public boolean isMultipass()
	{
		return true;
	}

	public void renderMultipass(LavaBoatEntity p_188300_1_, double p_188300_2_, double p_188300_4_, double p_188300_6_, float p_188300_8_, float p_188300_9_)
	{
		GlStateManager.pushMatrix();
		this.setupTranslation(p_188300_2_, p_188300_4_, p_188300_6_);
		this.setupRotation(p_188300_1_, p_188300_8_, p_188300_9_);
		this.bindEntityTexture(p_188300_1_);
		this.modelBoat.renderMultipass(p_188300_1_, p_188300_9_, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
		GlStateManager.popMatrix();
	}
}