package com.legacy.nethercraft.client.render.entity.tribal;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.entity.tribal.TribalTraineeEntity;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.layers.BipedArmorLayer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.util.ResourceLocation;

public class TribalTraineeRenderer<T extends TribalTraineeEntity, M extends BipedModel<T>> extends BipedRenderer<T, M>
{

	@SuppressWarnings("unchecked")
	public TribalTraineeRenderer(EntityRendererManager renderer)
	{
		super(renderer, (M) new BipedModel<TribalTraineeEntity>(), 0.5F);
		this.addLayer(new BipedArmorLayer<>(this, new BipedModel<>(0.5F), new BipedModel<>(1.0F)));
	}

	protected void preRenderCallback(TribalTraineeEntity entityliving, float f)
	{
		GlStateManager.scalef(0.65F, 0.65F, 0.65F);
	}

	@Override
	protected ResourceLocation getEntityTexture(TribalTraineeEntity entity)
	{
		return Nethercraft.locate("textures/entity/tribal/tribal_trainee.png");
	}
}