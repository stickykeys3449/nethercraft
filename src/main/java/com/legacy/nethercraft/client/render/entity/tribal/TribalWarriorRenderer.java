package com.legacy.nethercraft.client.render.entity.tribal;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.entity.tribal.TribalWarriorEntity;

import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.layers.BipedArmorLayer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.util.ResourceLocation;

public class TribalWarriorRenderer<T extends TribalWarriorEntity, M extends BipedModel<T>> extends BipedRenderer<T, M>
{
	@SuppressWarnings("unchecked")
	public TribalWarriorRenderer(EntityRendererManager renderer)
	{
		super(renderer, (M) new BipedModel<TribalWarriorEntity>(), 0.5F);
		this.addLayer(new BipedArmorLayer<>(this, new BipedModel<>(0.5F), new BipedModel<>(1.0F)));
	}

	@Override
	protected ResourceLocation getEntityTexture(TribalWarriorEntity entity)
	{
		return Nethercraft.locate("textures/entity/tribal/tribal_" + entity.getWarriorType() + ".png");
	}
}