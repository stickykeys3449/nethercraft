package com.legacy.nethercraft.client.render;

import com.legacy.nethercraft.client.render.tile_entity.NetherChestRenderer;
import com.legacy.nethercraft.tile_entity.NetherChestTileEntity;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.client.registry.ClientRegistry;

@OnlyIn(Dist.CLIENT)
public class NetherTileEntityRendering
{
	public static void init()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(NetherChestTileEntity.class, new NetherChestRenderer());
	}
}
