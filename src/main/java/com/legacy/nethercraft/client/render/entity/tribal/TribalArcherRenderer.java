package com.legacy.nethercraft.client.render.entity.tribal;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.entity.tribal.TribalArcherEntity;

import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.layers.BipedArmorLayer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.item.BowItem;
import net.minecraft.util.HandSide;
import net.minecraft.util.ResourceLocation;

public class TribalArcherRenderer<T extends TribalArcherEntity, M extends BipedModel<T>> extends BipedRenderer<T, M>
{
	@SuppressWarnings("unchecked")
	public TribalArcherRenderer(EntityRendererManager renderer)
	{
		super(renderer, (M) new BipedModel<TribalArcherEntity>(), 0.5F);
		this.addLayer(new BipedArmorLayer<>(this, new BipedModel<>(0.5F), new BipedModel<>(1.0F)));
	}

	protected void preRenderCallback(TribalArcherEntity entity, float f)
	{
		if (entity.isAggressive() && entity.getHeldItemMainhand().getItem() instanceof BowItem)
		{
			this.getEntityModel().leftArmPose = BipedModel.ArmPose.BOW_AND_ARROW;
			this.getEntityModel().rightArmPose = BipedModel.ArmPose.BOW_AND_ARROW;
		}
		else
		{
			if (entity.getPrimaryHand() == HandSide.RIGHT)
			{
				this.getEntityModel().rightArmPose = BipedModel.ArmPose.ITEM;
				this.getEntityModel().leftArmPose = BipedModel.ArmPose.EMPTY;
			}
			else
			{
				this.getEntityModel().rightArmPose = BipedModel.ArmPose.EMPTY;
				this.getEntityModel().leftArmPose = BipedModel.ArmPose.ITEM;
			}
		}
	}

	@Override
	protected ResourceLocation getEntityTexture(TribalArcherEntity entity)
	{
		return Nethercraft.locate("textures/entity/tribal/tribal_archer_" + entity.getWarriorType() + ".png");
	}
}