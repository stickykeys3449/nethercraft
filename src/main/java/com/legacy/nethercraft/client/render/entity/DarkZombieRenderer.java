package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.entity.DarkZombieEntity;

import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.layers.BipedArmorLayer;
import net.minecraft.client.renderer.entity.model.ZombieModel;
import net.minecraft.util.ResourceLocation;

public class DarkZombieRenderer<T extends DarkZombieEntity, M extends ZombieModel<T>> extends BipedRenderer<T, M>
{

	private static final ResourceLocation BLOODY_ZOMBIE_TEXTURE = new ResourceLocation("nethercraft", "textures/entity/zombie/zombie.png");

	@SuppressWarnings("unchecked")
	public DarkZombieRenderer(EntityRendererManager p_i50974_1_)
	{
		super(p_i50974_1_, (M) new ZombieModel<DarkZombieEntity>(), 0.5F);
		this.addLayer(new BipedArmorLayer<>(this, new ZombieModel<>(0.5F, true), new ZombieModel<>(1.0F, true)));
	}

	@Override
	protected ResourceLocation getEntityTexture(DarkZombieEntity entity)
	{
		return BLOODY_ZOMBIE_TEXTURE;
	}
}