package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.entity.LavaSlimeEntity;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.SlimeGelLayer;
import net.minecraft.client.renderer.entity.model.SlimeModel;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class LavaSlimeRenderer extends MobRenderer<LavaSlimeEntity, SlimeModel<LavaSlimeEntity>>
{

	private static final ResourceLocation ORANGE_SLIME_TEXTURE = new ResourceLocation("nethercraft", "textures/entity/slime/orange_slime.png");

    private static final ResourceLocation RED_SLIME_TEXTURE = new ResourceLocation("nethercraft", "textures/entity/slime/red_slime.png");


	public LavaSlimeRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn, new SlimeModel<>(16), 0.25F);
		this.addLayer(new SlimeGelLayer<>(this));
	}

	public void doRender(LavaSlimeEntity entity, double x, double y, double z, float entityYaw, float partialTicks)
	{
		this.shadowSize = 0.25F * (float) entity.getSlimeSize();
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
	}

	protected void preRenderCallback(LavaSlimeEntity entitylivingbaseIn, float partialTickTime)
	{
		GlStateManager.scalef(0.999F, 0.999F, 0.999F);
		float f1 = (float) entitylivingbaseIn.getSlimeSize();
		float f2 = MathHelper.lerp(partialTickTime, entitylivingbaseIn.prevSquishFactor, entitylivingbaseIn.squishFactor) / (f1 * 0.5F + 1.0F);
		float f3 = 1.0F / (f2 + 1.0F);
		GlStateManager.scalef(f3 * f1, 1.0F / f3 * f1, f3 * f1);
	}

	@Override
    protected ResourceLocation getEntityTexture(LavaSlimeEntity entity)
    {
        return ((LavaSlimeEntity)entity).getDataManager().get(LavaSlimeEntity.SLIME_TYPE) == 1 ? RED_SLIME_TEXTURE : ORANGE_SLIME_TEXTURE;
    }
}