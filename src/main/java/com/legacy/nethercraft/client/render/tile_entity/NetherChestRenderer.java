package com.legacy.nethercraft.client.render.tile_entity;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.tile_entity.NetherChestTileEntity;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.ChestBlock;
import net.minecraft.client.renderer.tileentity.ItemStackTileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.tileentity.model.ChestModel;
import net.minecraft.client.renderer.tileentity.model.LargeChestModel;
import net.minecraft.item.ItemStack;
import net.minecraft.state.properties.ChestType;
import net.minecraft.tileentity.IChestLid;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class NetherChestRenderer extends TileEntityRenderer<NetherChestTileEntity>
{
	private static final ResourceLocation glowood = Nethercraft.locate("textures/tile_entity/glowood.png");
	private static final ResourceLocation glowood_double = Nethercraft.locate("textures/tile_entity/glowood_double.png");

	private final ChestModel simpleChest = new ChestModel();
	private final ChestModel largeChest = new LargeChestModel();

	public NetherChestRenderer()
	{	
	}

	public void render(NetherChestTileEntity tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage)
	{
		GlStateManager.enableDepthTest();
		GlStateManager.depthFunc(515);
		GlStateManager.depthMask(true);
		BlockState state = tileEntityIn != null && tileEntityIn.hasWorld() ? tileEntityIn.getBlockState() : Blocks.CHEST.getDefaultState().with(ChestBlock.FACING, Direction.SOUTH);
		ChestType chestType = state.has(ChestBlock.TYPE) ? state.get(ChestBlock.TYPE) : ChestType.SINGLE;
		
		if (chestType != ChestType.LEFT)
		{
			boolean flag = chestType != ChestType.SINGLE;
			ChestModel chestmodel = this.getChestModel(tileEntityIn, destroyStage, flag);
			if (destroyStage >= 0)
			{
				GlStateManager.matrixMode(5890);
				GlStateManager.pushMatrix();
				GlStateManager.scalef(flag ? 8.0F : 4.0F, 4.0F, 1.0F);
				GlStateManager.translatef(0.0625F, 0.0625F, 0.0625F);
				GlStateManager.matrixMode(5888);
			}
			else
			{
				GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
			}

			GlStateManager.pushMatrix();
			GlStateManager.enableRescaleNormal();
			GlStateManager.translatef((float) x, (float) y + 1.0F, (float) z + 1.0F);
			GlStateManager.scalef(1.0F, -1.0F, -1.0F);
			float f = state.get(ChestBlock.FACING).getHorizontalAngle();
			if ((double) Math.abs(f) > 1.0E-5D)
			{
				GlStateManager.translatef(0.5F, 0.5F, 0.5F);
				GlStateManager.rotatef(f, 0.0F, 1.0F, 0.0F);
				GlStateManager.translatef(-0.5F, -0.5F, -0.5F);
			}

			this.applyLidRotation(tileEntityIn, partialTicks, chestmodel);
			chestmodel.renderAll();
			GlStateManager.disableRescaleNormal();
			GlStateManager.popMatrix();
			GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
			if (destroyStage >= 0)
			{
				GlStateManager.matrixMode(5890);
				GlStateManager.popMatrix();
				GlStateManager.matrixMode(5888);
			}

		}
	}

	private ChestModel getChestModel(NetherChestTileEntity tileEntityIn, int destroyStage, boolean doubleChest)
	{
		ResourceLocation resourceLocation;
		if (destroyStage >= 0)
		{
			resourceLocation = DESTROY_STAGES[destroyStage];
		}
		else
		{
			resourceLocation = doubleChest ? glowood_double : glowood;
		}

		this.bindTexture(resourceLocation);
		return doubleChest ? this.largeChest : this.simpleChest;
	}

	private void applyLidRotation(NetherChestTileEntity te, float angle, ChestModel model)
	{
		float f = ((IChestLid) te).getLidAngle(angle);
		f = 1.0F - f;
		f = 1.0F - f * f * f;
		model.getLid().rotateAngleX = -(f * ((float) Math.PI / 2F));
	}

	public static class NetherISTER extends ItemStackTileEntityRenderer
	{
		@Override
		public void renderByItem(ItemStack stack)
		{
			TileEntityRenderer<?> r = TileEntityRendererDispatcher.instance.getRenderer(NetherChestTileEntity.class);
			((NetherChestRenderer) r).render(NetherChestTileEntity.getFromBlock(Block.getBlockFromItem(stack.getItem())), 0, 0, 0, 0, -1);
		}
	}
}
