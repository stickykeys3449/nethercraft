package com.legacy.nethercraft.client.render.entity.projectile;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.entity.projectile.NetherrackArrowEntity;

import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class NetherrackArrowRenderer extends ArrowRenderer<NetherrackArrowEntity>
{

	public static final ResourceLocation ARROW = Nethercraft.locate("textures/entity/projectile/netherrack_arrow.png");

	public NetherrackArrowRenderer(EntityRendererManager manager)
	{
		super(manager);
	}

	protected ResourceLocation getEntityTexture(NetherrackArrowEntity entity)
	{
		return ARROW;
	}
}