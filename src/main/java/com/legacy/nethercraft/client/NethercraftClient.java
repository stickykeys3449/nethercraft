package com.legacy.nethercraft.client;

import com.legacy.nethercraft.client.render.NetherTileEntityRendering;

import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

public class NethercraftClient
{

	public static void initialization(FMLClientSetupEvent event)
	{
		NetherEntityRendering.init();
		NetherTileEntityRendering.init();
	}
}