package com.legacy.nethercraft.client;

import com.legacy.nethercraft.client.render.entity.BloodyZombieRenderer;
import com.legacy.nethercraft.client.render.entity.DarkZombieRenderer;
import com.legacy.nethercraft.client.render.entity.GhastBombRenderer;
import com.legacy.nethercraft.client.render.entity.ImpRenderer;
import com.legacy.nethercraft.client.render.entity.LavaBoatRenderer;
import com.legacy.nethercraft.client.render.entity.NetherPaintingRenderer;
import com.legacy.nethercraft.client.render.entity.LavaSlimeRenderer;
import com.legacy.nethercraft.client.render.entity.CamouflageSpiderRenderer;
import com.legacy.nethercraft.client.render.entity.SlimeEggRenderer;
import com.legacy.nethercraft.client.render.entity.projectile.LiniumArrowRenderer;
import com.legacy.nethercraft.client.render.entity.projectile.NeridiumArrowRenderer;
import com.legacy.nethercraft.client.render.entity.projectile.NetherrackArrowRenderer;
import com.legacy.nethercraft.client.render.entity.projectile.PyridiumArrowRenderer;
import com.legacy.nethercraft.client.render.entity.tribal.TribalArcherRenderer;
import com.legacy.nethercraft.client.render.entity.tribal.TribalTraineeRenderer;
import com.legacy.nethercraft.client.render.entity.tribal.TribalWarriorRenderer;
import com.legacy.nethercraft.entity.BloodyZombieEntity;
import com.legacy.nethercraft.entity.DarkZombieEntity;
import com.legacy.nethercraft.entity.ImpEntity;
import com.legacy.nethercraft.entity.LavaSlimeEntity;
import com.legacy.nethercraft.entity.CamouflageSpiderEntity;
import com.legacy.nethercraft.entity.misc.GhastBombEntity;
import com.legacy.nethercraft.entity.misc.LavaBoatEntity;
import com.legacy.nethercraft.entity.misc.NetherPaintingEntity;
import com.legacy.nethercraft.entity.projectile.LiniumArrowEntity;
import com.legacy.nethercraft.entity.projectile.NeridiumArrowEntity;
import com.legacy.nethercraft.entity.projectile.NetherrackArrowEntity;
import com.legacy.nethercraft.entity.projectile.PyridiumArrowEntity;
import com.legacy.nethercraft.entity.projectile.SlimeEggEntity;
import com.legacy.nethercraft.entity.tribal.TribalArcherEntity;
import com.legacy.nethercraft.entity.tribal.TribalTraineeEntity;
import com.legacy.nethercraft.entity.tribal.TribalWarriorEntity;

import net.minecraft.entity.Entity;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class NetherEntityRendering
{

	public static void init()
	{
		register(DarkZombieEntity.class, DarkZombieRenderer::new);
		register(BloodyZombieEntity.class, BloodyZombieRenderer::new);
		register(CamouflageSpiderEntity.class, CamouflageSpiderRenderer::new);
		register(ImpEntity.class, ImpRenderer::new);
		register(LavaSlimeEntity.class, LavaSlimeRenderer::new);

		register(TribalWarriorEntity.class, TribalWarriorRenderer::new);
		register(TribalArcherEntity.class, TribalArcherRenderer::new);
		register(TribalTraineeEntity.class, TribalTraineeRenderer::new);

		register(NetherrackArrowEntity.class, NetherrackArrowRenderer::new);
		register(NeridiumArrowEntity.class, NeridiumArrowRenderer::new);
		register(PyridiumArrowEntity.class, PyridiumArrowRenderer::new);
		register(LiniumArrowEntity.class, LiniumArrowRenderer::new);

		register(SlimeEggEntity.class, SlimeEggRenderer::new);
		register(GhastBombEntity.class, GhastBombRenderer::new);
		register(NetherPaintingEntity.class, NetherPaintingRenderer::new);
		register(LavaBoatEntity.class, LavaBoatRenderer::new);
	}

	private static <T extends Entity> void register(Class<T> entityClass, IRenderFactory<? super T> renderFactory)
	{
		RenderingRegistry.registerEntityRenderingHandler(entityClass, renderFactory);
	}
}