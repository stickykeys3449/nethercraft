package com.legacy.nethercraft.item.util;

import java.util.function.Supplier;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.LazyLoadBase;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public enum NetherArmorMaterial implements IArmorMaterial
{
	IMP_SKIN("imp_skin", 5, new int[] { 1, 2, 3, 1 }, 15, SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0.0F, () -> {
		return Ingredient.fromItems(NetherItems.imp_skin);
	}), NERIDIUM("neridium", 15, new int[] { 2, 5, 6, 2 }, 9, SoundEvents.ITEM_ARMOR_EQUIP_IRON, 0.0F, () -> {
		return Ingredient.fromItems(NetherItems.neridium_ingot);
	}), W_OBSIDIAN("w_obsidian", 40, new int[] { 3, 6, 8, 3 }, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0F, () -> {
		return Ingredient.fromItems(NetherItems.w_obsidian_ingot);
	});

	private static final int[] MAX_DAMAGE_ARRAY = new int[] { 13, 15, 16, 11 };

	private final String name;

	private final int maxDamageFactor;

	private final int[] damageReductionAmountArray;

	private final int enchantability;

	private final SoundEvent soundEvent;

	private final float toughness;

	private final LazyLoadBase<Ingredient> repairMaterial;

	private NetherArmorMaterial(String nameIn, int maxDamageFactorIn, int[] reductionAmountIn, int enchantabilityIn, SoundEvent soundIn, float toughnessIn, Supplier<Ingredient> p_i48533_9_)
	{
		this.name = Nethercraft.find(nameIn);
		this.maxDamageFactor = maxDamageFactorIn;
		this.damageReductionAmountArray = reductionAmountIn;
		this.enchantability = enchantabilityIn;
		this.soundEvent = soundIn;
		this.toughness = toughnessIn;
		this.repairMaterial = new LazyLoadBase<>(p_i48533_9_);
	}

	public int getDurability(EquipmentSlotType slotIn)
	{
		return MAX_DAMAGE_ARRAY[slotIn.getIndex()] * this.maxDamageFactor;
	}

	public int getDamageReductionAmount(EquipmentSlotType slotIn)
	{
		return this.damageReductionAmountArray[slotIn.getIndex()];
	}

	public int getEnchantability()
	{
		return this.enchantability;
	}

	public SoundEvent getSoundEvent()
	{
		return this.soundEvent;
	}

	public Ingredient getRepairMaterial()
	{
		return this.repairMaterial.getValue();
	}

	@OnlyIn(Dist.CLIENT)
	public String getName()
	{
		return this.name;
	}

	public float getToughness()
	{
		return this.toughness;
	}
}