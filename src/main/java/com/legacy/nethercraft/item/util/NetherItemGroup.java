package com.legacy.nethercraft.item.util;

import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.block.Blocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public abstract class NetherItemGroup
{
	public static final ItemGroup BLOCKS = new ItemGroup("nether_blocks")
	{
		@Override
		@OnlyIn(Dist.CLIENT)
		public ItemStack createIcon() 
		{
			return new ItemStack(Blocks.NETHERRACK);
		}
	};
	
	public static final ItemGroup TOOLS = new ItemGroup("nether_tools")
	{
		@Override
		@OnlyIn(Dist.CLIENT)
		public ItemStack createIcon() 
		{
			return new ItemStack(NetherItems.glowood_pickaxe);
		}
	};
	
	public static final ItemGroup COMBAT = new ItemGroup("nether_combat")
	{
		@Override
		@OnlyIn(Dist.CLIENT)
		public ItemStack createIcon() 
		{
			return new ItemStack(NetherItems.pyridium_sword);
		}
	};

	public static final ItemGroup MISC = new ItemGroup("nether_misc")
	{
		@Override
		@OnlyIn(Dist.CLIENT)
		public ItemStack createIcon() 
		{
			return new ItemStack(NetherItems.red_feather);
		}
	};
}