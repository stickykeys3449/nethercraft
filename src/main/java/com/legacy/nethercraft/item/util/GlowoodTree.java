package com.legacy.nethercraft.item.util;

import java.util.Random;

import javax.annotation.Nullable;

import com.legacy.nethercraft.world.feature.GlowoodTreeFeature;

import net.minecraft.block.trees.Tree;
import net.minecraft.world.gen.feature.AbstractTreeFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

public class GlowoodTree extends Tree
{

	@Nullable
	protected AbstractTreeFeature<NoFeatureConfig> getTreeFeature(Random random)
	{
		return (AbstractTreeFeature<NoFeatureConfig>) new GlowoodTreeFeature(NoFeatureConfig::deserialize, true);
	}
}