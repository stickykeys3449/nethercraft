package com.legacy.nethercraft.item.util;

import java.util.function.Supplier;

import com.legacy.nethercraft.block.NetherBlocks;
import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.block.Blocks;
import net.minecraft.item.IItemTier;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.LazyLoadBase;

public enum NetherItemTier implements IItemTier
{
   GLOWWOOD(0, 160, 2.0F, 0.0F, 15, () -> {
      return Ingredient.fromItems(NetherBlocks.glowood_planks);
   }),
   NETHERRACK(1, 320, 4.0F, 1.0F, 5, () -> {
      return Ingredient.fromItems(Blocks.NETHERRACK);
   }),
   NERIDIUM(2, 540, 6.0F, 2.0F, 14, () -> {
      return Ingredient.fromItems(NetherItems.neridium_ingot);
   }),
   PYRIDIUM(3, 2064, 8.0F, 3.0F, 10, () -> {
      return Ingredient.fromItems(NetherItems.pyridium_ingot);
   }),
   LINIUM(3, 320, 12.0F, 3.0F, 7, () -> {
      return Ingredient.fromItems(NetherItems.linium_ingot);
   });

	private final int harvestLevel;

	private final int maxUses;

	private final float efficiency;

	private final float attackDamage;

	private final int enchantability;

	private final LazyLoadBase<Ingredient> repairMaterial;

	private NetherItemTier(int harvestLevelIn, int maxUsesIn, float efficiencyIn, float attackDamageIn, int enchantabilityIn, Supplier<Ingredient> repairMaterialIn)
	{
		this.harvestLevel = harvestLevelIn;
		this.maxUses = maxUsesIn;
		this.efficiency = efficiencyIn;
		this.attackDamage = attackDamageIn;
		this.enchantability = enchantabilityIn;
		this.repairMaterial = new LazyLoadBase<>(repairMaterialIn);
	}

	public int getMaxUses()
	{
		return this.maxUses;
	}

	public float getEfficiency()
	{
		return this.efficiency;
	}

	public float getAttackDamage()
	{
		return this.attackDamage;
	}

	public int getHarvestLevel()
	{
		return this.harvestLevel;
	}

	public int getEnchantability()
	{
		return this.enchantability;
	}

	public Ingredient getRepairMaterial()
	{
		return this.repairMaterial.getValue();
	}
}