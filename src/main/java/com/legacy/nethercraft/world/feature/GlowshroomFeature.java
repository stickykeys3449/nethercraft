package com.legacy.nethercraft.world.feature;

import java.util.Random;
import java.util.function.Function;

import com.legacy.nethercraft.block.NetherBlocks;
import com.mojang.datafixers.Dynamic;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.feature.FlowersFeature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

public class GlowshroomFeature extends FlowersFeature
{
	public GlowshroomFeature(Function<Dynamic<?>, ? extends NoFeatureConfig> p_i49876_1_)
	{
		super(p_i49876_1_);
	}

	@Override
	public BlockState getRandomFlower(Random rand, BlockPos pos)
	{
		if (rand.nextBoolean())
		{
			return NetherBlocks.green_glowshroom.getDefaultState();
		}
		else
		{
			return NetherBlocks.purple_glowshroom.getDefaultState();
		}
	}
}