package com.legacy.nethercraft.world;

import java.util.Random;

import com.legacy.nethercraft.block.NetherBlocks;
import com.legacy.nethercraft.entity.BloodyZombieEntity;
import com.legacy.nethercraft.entity.DarkZombieEntity;
import com.legacy.nethercraft.entity.NetherEntityTypes;
import com.legacy.nethercraft.entity.goal.PigmanRangedBowAttackGoal;
import com.legacy.nethercraft.entity.tribal.TribalEntity;
import com.legacy.nethercraft.item.NetherItems;
import com.legacy.nethercraft.world.feature.GlowoodTreeFeature;
import com.legacy.nethercraft.world.feature.GlowshroomFeature;
import com.legacy.nethercraft.world.feature.LavaReedFeature;

import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.goal.ZombieAttackGoal;
import net.minecraft.entity.monster.GhastEntity;
import net.minecraft.entity.monster.ZombiePigmanEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IndirectEntityDamageSource;
import net.minecraft.world.GameRules;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig.FillerBlockType;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.FrequencyConfig;
import net.minecraft.world.gen.placement.NoiseDependant;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.surfacebuilders.ConfiguredSurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingSetAttackTargetEvent;
import net.minecraftforge.event.entity.living.ZombieEvent.SummonAidEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.registries.ForgeRegistries;

public class NetherEvents
{
	private Random rand;

	public NetherEvents()
	{
		this.rand = new Random();
	}

	public static void setupNetherFeatures(FMLCommonSetupEvent event)
	{
		for (Biome biome : ForgeRegistries.BIOMES.getValues())
		{
			if (biome == Biomes.NETHER)
			{
				// Literally everything from mob spawning, and biome decoration
				biome.addFeature(GenerationStage.Decoration.UNDERGROUND_DECORATION, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(FillerBlockType.NETHERRACK, NetherBlocks.foulite_ore.getDefaultState(), 14), Placement.COUNT_RANGE, new CountRangeConfig(20, 10, 20, 128)));
				biome.addFeature(GenerationStage.Decoration.UNDERGROUND_DECORATION, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(FillerBlockType.NETHERRACK, NetherBlocks.neridium_ore.getDefaultState(), 8), Placement.COUNT_RANGE, new CountRangeConfig(8, 10, 20, 128)));
				biome.addFeature(GenerationStage.Decoration.UNDERGROUND_DECORATION, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(FillerBlockType.NETHERRACK, NetherBlocks.pyridium_ore.getDefaultState(), 5), Placement.COUNT_RANGE, new CountRangeConfig(4, 10, 20, 128)));
				biome.addFeature(GenerationStage.Decoration.UNDERGROUND_DECORATION, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(FillerBlockType.NETHERRACK, NetherBlocks.linium_ore.getDefaultState(), 3), Placement.COUNT_RANGE, new CountRangeConfig(5, 10, 20, 128)));
				biome.addFeature(GenerationStage.Decoration.UNDERGROUND_DECORATION, Biome.createDecoratedFeature(Feature.ORE, new OreFeatureConfig(FillerBlockType.NETHERRACK, NetherBlocks.w_ore.getDefaultState(), 4), Placement.COUNT_RANGE, new CountRangeConfig(2, 10, 20, 128)));

				biome.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Biome.createDecoratedFeature(new GlowoodTreeFeature(NoFeatureConfig::deserialize, false), IFeatureConfig.NO_FEATURE_CONFIG, Placement.COUNT_RANGE, new CountRangeConfig(40, 30, 0, 50)));
				biome.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Biome.createDecoratedFeature(new LavaReedFeature(NoFeatureConfig::deserialize), IFeatureConfig.NO_FEATURE_CONFIG, Placement.COUNT_HEIGHTMAP_DOUBLE, new FrequencyConfig(20)));
				biome.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Biome.createDecoratedFeature(new GlowshroomFeature(NoFeatureConfig::deserialize), IFeatureConfig.NO_FEATURE_CONFIG, Placement.NOISE_HEIGHTMAP_32, new NoiseDependant(-0.8D, 15, 4)));

				biome.getSpawns(EntityClassification.CREATURE).add(new Biome.SpawnListEntry(NetherEntityTypes.IMP, 30, 2, 3));

				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.DARK_ZOMBIE, 50, 0, 2));
				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.BLOODY_ZOMBIE, 50, 0, 2));
				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.CAMOUFLAGE_SPIDER, 60, 1, 3));
				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.LAVA_SLIME, 40, 1, 2));

				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.TRIBAL_WARRIOR, 50, 0, 3));
				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.TRIBAL_ARCHER, 50, 1, 2));
				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.TRIBAL_TRAINEE, 50, 1, 3));

				NetherEvents.setNetherBuilder(biome, new ConfiguredSurfaceBuilder<>(new NetherDirtSurfaceBuilder(SurfaceBuilderConfig::deserialize), SurfaceBuilder.NETHERRACK_CONFIG));
			}
		}
	}

	@SubscribeEvent
	public void livingSpawn(EntityJoinWorldEvent event)
	{
		if (event.getEntity() instanceof ZombiePigmanEntity)
		{
			if (this.rand.nextInt(5) == 0 && !((ZombiePigmanEntity) event.getEntity()).isChild())
			{
				event.getEntity().setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(NetherItems.pyridium_bow));
				((ZombiePigmanEntity) event.getEntity()).goalSelector.addGoal(1, new PigmanRangedBowAttackGoal<ZombiePigmanEntity>((ZombiePigmanEntity) event.getEntity(), 1.0D, 20, 15.0F));
				((ZombiePigmanEntity) event.getEntity()).getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(30.0F);
				((ZombiePigmanEntity) event.getEntity()).setHealth(30.0F);
			}
		}
	}

	@SubscribeEvent
	public void onEntityUpdate(LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof ZombiePigmanEntity)
		{
			ZombiePigmanEntity pigman = (ZombiePigmanEntity) event.getEntityLiving();
			boolean removedMelee = false;

			if (pigman.getHeldItemMainhand().getItem() instanceof BowItem && !removedMelee)
			{
				pigman.goalSelector.removeGoal(new ZombieAttackGoal((ZombiePigmanEntity) event.getEntity(), 1.0D, false));
				removedMelee = true;
			}

			if (pigman.getAttackTarget() != null && pigman.getAttackTarget() instanceof TribalEntity && (pigman.getAttackTarget().getHealth() <= 0 || !pigman.getAttackTarget().isAlive()))
			{
				this.removePigmanAnger(pigman);
			}
		}
	}

	@SubscribeEvent
	public void onEntityDeath(LivingDeathEvent event)
	{
		if (event.getEntityLiving() instanceof GhastEntity && event.getEntityLiving().world.rand.nextInt(3) == 0 && event.getEntityLiving().world.getGameRules().getBoolean(GameRules.DO_ENTITY_DROPS))
		{
			ItemStack bones = new ItemStack(NetherItems.ghast_bones);
			bones.setCount(event.getEntityLiving().world.rand.nextInt(2) + 1);
			event.getEntityLiving().entityDropItem(bones);
		}

		/*if (event.getEntityLiving() instanceof TribalEntity || event.getEntityLiving() instanceof ZombiePigmanEntity && event.getSource().getTrueSource() instanceof TribalEntity)
		{
			List<ZombiePigmanEntity> list = event.getEntityLiving().world.<ZombiePigmanEntity>getEntitiesWithinAABB(ZombiePigmanEntity.class, event.getEntityLiving().getBoundingBox().grow(30.0D, 15.0D, 30.0D));
		
			for (ZombiePigmanEntity surroundingPigmen : list)
			{
				if (surroundingPigmen.getAttackTarget() == null && surroundingPigmen.canEntityBeSeen(event.getEntityLiving()))
				{
					this.removePigmanAnger(surroundingPigmen);
				}
			}
		}*/
	}

	@SubscribeEvent
	public void onEntityAttack(LivingAttackEvent event)
	{
		if (event.getEntityLiving() instanceof ZombiePigmanEntity)
		{
			if (event.getSource() instanceof IndirectEntityDamageSource && event.getSource().getTrueSource() instanceof ZombiePigmanEntity)
			{
				event.getSource().getImmediateSource().remove();
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public void onAttackTarget(LivingSetAttackTargetEvent event)
	{
		if (event.getEntityLiving() instanceof ZombiePigmanEntity && event.getTarget() instanceof TribalEntity && !event.getEntityLiving().canEntityBeSeen(event.getTarget()))
		{
			this.removePigmanAnger((ZombiePigmanEntity) event.getEntityLiving());
		}
	}

	@SubscribeEvent
	public void onZombieSummoned(SummonAidEvent event)
	{
		if (event.getEntity() instanceof DarkZombieEntity)
		{
			event.setCustomSummonedAid(NetherEntityTypes.DARK_ZOMBIE.create(event.getWorld()));
		}

		if (event.getEntity() instanceof BloodyZombieEntity)
		{
			event.setCustomSummonedAid(NetherEntityTypes.BLOODY_ZOMBIE.create(event.getWorld()));
		}
	}

	/*@SubscribeEvent
	public void onPlayerBreakSpeed(PlayerEvent.BreakSpeed event)
	{
		if (event.getPlayer().getHeldItemMainhand().getItem() == NetherItems.linium_pickaxe && event.getState().getBlock() == Blocks.NETHERRACK)
		{
			event.setNewSpeed(event.getOriginalSpeed() - 2.0F);
		}
	}*/

	private void removePigmanAnger(ZombiePigmanEntity pigman)
	{
		ObfuscationReflectionHelper.setPrivateValue(ZombiePigmanEntity.class, pigman, 0, "field_70837_d");
		pigman.setRevengeTarget(null);
		pigman.setAttackTarget(null);
	}

	public static void setNetherBuilder(Biome biome, ConfiguredSurfaceBuilder<?> builder)
	{
		try
		{
			ObfuscationReflectionHelper.setPrivateValue(Biome.class, biome, builder, "field_201875_ar");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}