package com.legacy.nethercraft.tile_entity;

import net.minecraft.block.Block;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class NetherChestTileEntity extends ChestTileEntity
{

	public static class GlowoodChestTileEntity extends NetherChestTileEntity
	{

		public GlowoodChestTileEntity()
		{
			super(NetherTileEntityTypes.GLOWOOD_CHEST);
		}
	}

	private final TranslationTextComponent localizedName;

	public NetherChestTileEntity(TileEntityType<?> typeIn)
	{
		super(typeIn);
		this.localizedName = new TranslationTextComponent("block.nethercraft." + typeIn.getRegistryName().getPath());
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public AxisAlignedBB getRenderBoundingBox()
    {
        return new AxisAlignedBB(this.pos.add(-1, 0, -1), this.pos.add(2, 2, 2));
    }

	public static NetherChestTileEntity getFromBlock(Block blockIn)
	{
		NetherChestTileEntity tile;
		//if (blockIn == NetherBlocks.glowood_chest)
		{
			tile = new GlowoodChestTileEntity();
		}
			
		return tile;
	}

	protected ITextComponent getDefaultName()
	{
		return localizedName;
	}
}
