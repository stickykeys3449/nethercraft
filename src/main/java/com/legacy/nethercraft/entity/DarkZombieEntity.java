package com.legacy.nethercraft.entity;

import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.world.Difficulty;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;

public class DarkZombieEntity extends ZombieEntity
{

	public DarkZombieEntity(EntityType<? extends DarkZombieEntity> type, World world)
	{
		super(type, world);
	}

	protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(50.0D);
	}

	@Override
	protected boolean shouldBurnInDay()
	{
		return false;
	}

	@Override
	protected boolean shouldDrown()
	{
		return false;
	}

	@Override
	protected void setEquipmentBasedOnDifficulty(DifficultyInstance difficulty)
	{
		if (this.rand.nextFloat() < 0.15F * difficulty.getClampedAdditionalDifficulty())
		{
			int i = this.rand.nextInt(2);
			float f = this.world.getDifficulty() == Difficulty.HARD ? 0.1F : 0.25F;
			if (this.rand.nextFloat() < 0.095F)
			{
				++i;
			}

			if (this.rand.nextFloat() < 0.095F)
			{
				++i;
			}

			if (this.rand.nextFloat() < 0.095F)
			{
				++i;
			}

			boolean flag = true;

			for (EquipmentSlotType equipmentslottype : EquipmentSlotType.values())
			{
				if (equipmentslottype.getSlotType() == EquipmentSlotType.Group.ARMOR)
				{
					ItemStack itemstack = this.getItemStackFromSlot(equipmentslottype);
					if (!flag && this.rand.nextFloat() < f)
					{
						break;
					}

					flag = false;
					if (itemstack.isEmpty())
					{
						Item item = getArmorByChance(equipmentslottype, i);
						if (item != null)
						{
							this.setItemStackToSlot(equipmentslottype, new ItemStack(item));
						}
					}
				}
			}
		}

		if (this.rand.nextFloat() < (this.world.getDifficulty() == Difficulty.HARD ? 0.05F : 0.01F))
		{
			int i = this.rand.nextInt(3);
			if (i == 0)
			{
				this.setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(NetherItems.neridium_sword));
			}
			else
			{
				this.setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(NetherItems.neridium_shovel));
			}
		}
	}

	public static Item getArmorByChance(EquipmentSlotType slotIn, int chance)
	{
		switch (slotIn)
		{
		case HEAD:
			if (chance == 0)
			{
				return NetherItems.imp_helmet;
			}
			else if (chance == 1)
			{
				return Items.GOLDEN_HELMET;
			}
			else if (chance == 2)
			{
				return Items.CHAINMAIL_HELMET;
			}
			else if (chance == 3)
			{
				return NetherItems.neridium_helmet;
			}
			else if (chance == 4)
			{
				return NetherItems.w_obsidian_helmet;
			}
		case CHEST:
			if (chance == 0)
			{
				return NetherItems.imp_chestplate;
			}
			else if (chance == 1)
			{
				return Items.GOLDEN_CHESTPLATE;
			}
			else if (chance == 2)
			{
				return Items.CHAINMAIL_CHESTPLATE;
			}
			else if (chance == 3)
			{
				return NetherItems.neridium_chestplate;
			}
			else if (chance == 4)
			{
				return NetherItems.w_obsidian_chestplate;
			}
		case LEGS:
			if (chance == 0)
			{
				return NetherItems.imp_leggings;
			}
			else if (chance == 1)
			{
				return Items.GOLDEN_LEGGINGS;
			}
			else if (chance == 2)
			{
				return Items.CHAINMAIL_LEGGINGS;
			}
			else if (chance == 3)
			{
				return NetherItems.neridium_leggings;
			}
			else if (chance == 4)
			{
				return NetherItems.w_obsidian_leggings;
			}
		case FEET:
			if (chance == 0)
			{
				return NetherItems.imp_boots;
			}
			else if (chance == 1)
			{
				return Items.GOLDEN_BOOTS;
			}
			else if (chance == 2)
			{
				return Items.CHAINMAIL_BOOTS;
			}
			else if (chance == 3)
			{
				return NetherItems.neridium_boots;
			}
			else if (chance == 4)
			{
				return NetherItems.w_obsidian_boots;
			}
		default:
			return null;
		}
	}
}