package com.legacy.nethercraft.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.SpiderEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.World;

public class CamouflageSpiderEntity extends SpiderEntity
{

	public static final DataParameter<Integer> SPIDER_TYPE = EntityDataManager.<Integer> createKey(CamouflageSpiderEntity.class, DataSerializers.VARINT);

	public CamouflageSpiderEntity(EntityType<? extends CamouflageSpiderEntity> type, World worldIn)
	{
		super(type, worldIn);
		this.dataManager.set(SPIDER_TYPE, this.rand.nextInt(2));
	}

	@Override
	protected void registerData()
	{
		super.registerData();
		this.dataManager.register(SPIDER_TYPE, Integer.valueOf(this.rand.nextInt(2)));
	}

	protected void registerAttributes()
	{
		super.registerAttributes();
		double baseHealth = this.dataManager.get(SPIDER_TYPE) == 1 ? 40.0D : 35.0D;
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(baseHealth);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3F);
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("spider_type", this.dataManager.get(SPIDER_TYPE));
	}

	@Override
	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);
		this.dataManager.set(SPIDER_TYPE, compound.getInt("spider_type"));
	}
}