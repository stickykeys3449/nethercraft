package com.legacy.nethercraft.entity.projectile;

import com.legacy.nethercraft.entity.NetherEntityTypes;
import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;

public class NetherrackArrowEntity extends AbstractArrowEntity
{
	public NetherrackArrowEntity(EntityType<? extends NetherrackArrowEntity> p_i50158_1_, World p_i50158_2_)
	{
		super(p_i50158_1_, p_i50158_2_);
		this.setNoGravity(true);
	}

	public NetherrackArrowEntity(World worldIn, LivingEntity shooter)
	{
		super(NetherEntityTypes.NETHERRACK_ARROW, shooter, worldIn);
		this.setNoGravity(true);
	}

	public NetherrackArrowEntity(World worldIn, double x, double y, double z)
	{
		super(NetherEntityTypes.NETHERRACK_ARROW, x, y, z, worldIn);
		this.setNoGravity(true);
	}

	public NetherrackArrowEntity(FMLPlayMessages.SpawnEntity spawnEntity, World world)
	{
		this(NetherEntityTypes.NETHERRACK_ARROW, world);
	}

	public void tick()
	{
		super.tick();
		if (this.world.isRemote && !this.inGround)
		{
			this.world.addParticle(ParticleTypes.SMOKE, this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
		}

		boolean flag = this.func_203047_q();
		Vec3d vec3d = this.getMotion();
		float f1 = 0.99F;
		this.setMotion(vec3d.scale((double) f1));
		if (!flag)
		{
			Vec3d vec3d3 = this.getMotion();
			this.setMotion(vec3d3.x, vec3d3.y - (double) 0.03F, vec3d3.z);
		}
	}

	protected ItemStack getArrowStack()
	{
		return new ItemStack(NetherItems.netherrack_arrow);
	}

	protected void arrowHit(LivingEntity living)
	{
		super.arrowHit(living);
		//EffectInstance effectinstance = new EffectInstance(Effects.GLOWING, this.duration, 0);
		//living.addPotionEffect(effectinstance);
	}

	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);
	}

	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
	}

	@Override
	public IPacket<?> createSpawnPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}