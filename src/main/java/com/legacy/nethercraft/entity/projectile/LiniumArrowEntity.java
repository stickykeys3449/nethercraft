package com.legacy.nethercraft.entity.projectile;

import com.legacy.nethercraft.entity.NetherEntityTypes;
import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;

public class LiniumArrowEntity extends AbstractArrowEntity
{

	public LiniumArrowEntity(EntityType<? extends LiniumArrowEntity> p_i50158_1_, World p_i50158_2_)
	{
		super(p_i50158_1_, p_i50158_2_);
		this.setNoGravity(true);
	}

	public LiniumArrowEntity(World worldIn, LivingEntity shooter)
	{
		super(NetherEntityTypes.LINIUM_ARROW, shooter, worldIn);
		this.setNoGravity(true);
	}

	public LiniumArrowEntity(World worldIn, double x, double y, double z)
	{
		super(NetherEntityTypes.LINIUM_ARROW, x, y, z, worldIn);
		this.setNoGravity(true);
	}

	public LiniumArrowEntity(FMLPlayMessages.SpawnEntity spawnEntity, World world)
	{
		this(NetherEntityTypes.LINIUM_ARROW, world);
	}

	public void tick()
	{
		super.tick();
		if (this.world.isRemote && !this.inGround)
		{
			this.world.addParticle(ParticleTypes.SMOKE, this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
			
			if (this.ticksExisted > 1000)
			{
				this.remove();
			}
		}
	}

	protected ItemStack getArrowStack()
	{
		return new ItemStack(NetherItems.linium_arrow);
	}

	protected void arrowHit(LivingEntity living)
	{
		super.arrowHit(living);
	}

	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);
	}

	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
	}

	@Override
	public IPacket<?> createSpawnPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}