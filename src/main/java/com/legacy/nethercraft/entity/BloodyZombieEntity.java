package com.legacy.nethercraft.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.world.World;

public class BloodyZombieEntity extends DarkZombieEntity
{

	public BloodyZombieEntity(EntityType<? extends BloodyZombieEntity> type, World world)
	{
		super(type, world);
	}

	protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(35.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.28000000417232513D);
	}
}