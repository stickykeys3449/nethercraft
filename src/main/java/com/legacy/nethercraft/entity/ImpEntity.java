package com.legacy.nethercraft.entity;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.Pose;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.PanicGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.Explosion.Mode;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootTables;

public class ImpEntity extends CreatureEntity
{

	public static final DataParameter<Integer> IMP_TYPE = EntityDataManager.<Integer> createKey(ImpEntity.class, DataSerializers.VARINT);

	public ImpEntity(EntityType<? extends ImpEntity> type, World worldIn)
	{
		super(NetherEntityTypes.IMP, worldIn);
		this.setImpType(this.rand.nextInt(3));
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new SwimGoal(this));
		this.goalSelector.addGoal(1, new PanicGoal(this, 1.25D));
		this.goalSelector.addGoal(6, new WaterAvoidingRandomWalkingGoal(this, 1.0D));
		this.goalSelector.addGoal(7, new LookAtGoal(this, PlayerEntity.class, 6.0F));
		this.goalSelector.addGoal(8, new LookRandomlyGoal(this));
	}

	@Override
	protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(30.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.25D);
	}

	@Override
	protected void registerData()
	{
		super.registerData();
		this.dataManager.register(IMP_TYPE, Integer.valueOf(this.rand.nextInt(2)));
	}

	public EntitySize getSize(Pose poseIn)
	{
		if (this.getImpType() == 0)
		{
			return super.getSize(poseIn).scale(0.8F);
		}
		else if (this.getImpType() == 1)
		{
			return super.getSize(poseIn).scale(1.2F);
		}
		else if (this.getImpType() == 2)
		{
			return super.getSize(poseIn).scale(1.45F);
		}
		else
		{
			return super.getSize(poseIn);
		}
	}

	@Override
	public void onDeath(DamageSource cause)
	{
		if (!this.world.isRemote)
		{
			if (this.getImpType() == 0)
			{
				this.world.createExplosion(this, posX, posY, posZ, 1.8F, false, Mode.NONE);
			}
			else if (this.getImpType() == 1)
			{
				for (int amount = 0; amount < 2; amount++)
				{
					ImpEntity entityimp = NetherEntityTypes.IMP.create(this.world);
					entityimp.setImpType(0);
					entityimp.setLocationAndAngles(this.posX + this.rand.nextFloat(), this.posY + 0.5D, this.posZ + this.rand.nextFloat(), this.rand.nextFloat() * 360F, 0.0F);
					this.world.addEntity(entityimp);
				}
			}
			else if (this.getImpType() == 2)
			{
				for (int amount = 0; amount < 2; amount++)
				{
					ImpEntity entityimp = NetherEntityTypes.IMP.create(this.world);
					entityimp.setImpType(1);
					entityimp.setLocationAndAngles(this.posX + this.rand.nextFloat(), this.posY + 0.5D, this.posZ + this.rand.nextFloat(), this.rand.nextFloat() * 360F, 0.0F);
					this.world.addEntity(entityimp);
				}
			}
		}
		super.onDeath(cause);
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("impType", this.getImpType());
	}

	@Override
	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);
		this.setImpType(compound.getInt("impType"));
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return SoundEvents.ENTITY_PIG_AMBIENT;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return SoundEvents.ENTITY_PIG_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return SoundEvents.ENTITY_PIG_DEATH;
	}

	public void setImpType(int type)
	{
		this.recalculateSize();
		this.dataManager.set(IMP_TYPE, type);
	}

	public int getImpType()
	{
		return this.dataManager.get(IMP_TYPE);
	}

	@Override
	public void notifyDataManagerChange(DataParameter<?> key)
	{
		if (IMP_TYPE.equals(key))
		{
			this.recalculateSize();
			this.rotationYaw = this.rotationYawHead;
			this.renderYawOffset = this.rotationYawHead;
			if (this.isInWater() && this.rand.nextInt(20) == 0)
			{
				this.doWaterSplashEffect();
			}
		}
		super.notifyDataManagerChange(key);
	}

	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return 0.825F * sizeIn.height;
	}

	protected ResourceLocation getLootTable()
	{
		return this.getImpType() == 0 ? this.getType().getLootTable() : LootTables.EMPTY;
	}

	@Override
	public ItemEntity entityDropItem(ItemStack stack, float offsetY)
	{
		if (stack.isEmpty())
		{
			return null;
		}
		else if (this.world.isRemote)
		{
			return null;
		}
		else
		{
			ItemEntity itementity = new ItemEntity(this.world, this.posX, this.posY + (double) offsetY, this.posZ, stack);
			itementity.setDefaultPickupDelay();
			itementity.setInvulnerable(true);
			itementity.setMotion(0, 0.3F, 0);
			if (captureDrops() != null)
				captureDrops().add(itementity);
			else
				this.world.addEntity(itementity);
			return itementity;
		}
	}

	public boolean canDespawn(double distanceToClosestPlayer)
	{
		return false;
	}
}