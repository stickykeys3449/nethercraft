package com.legacy.nethercraft.entity.tribal;

import com.legacy.nethercraft.entity.goal.TribalRangedBowAttackGoal;
import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;

public class TribalArcherEntity extends TribalEntity implements IRangedAttackMob
{

	public static final DataParameter<Integer> WARRIOR_TYPE = EntityDataManager.<Integer> createKey(TribalArcherEntity.class, DataSerializers.VARINT);

	public TribalArcherEntity(EntityType<? extends TribalArcherEntity> type, World world)
	{
		super(type, world);
	}

	@Override
	protected void registerGoals()
	{
		super.registerGoals();
		this.goalSelector.addGoal(2, new TribalRangedBowAttackGoal<>(this, 1.0D, 20, 15.0F));
	}

	protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(25.0D);
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("WarriorType", this.getWarriorType());
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);
		this.setWarriorType(compound.getInt("WarriorType"));
	}

	@Override
	protected void registerData()
	{
		super.registerData();
		this.dataManager.register(WARRIOR_TYPE, Integer.valueOf(this.rand.nextInt(2)));
	}

	protected void setEquipmentBasedOnDifficulty(DifficultyInstance difficulty)
	{
		this.setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(NetherItems.neridium_bow));
	}

	public void setWarriorType(int type)
	{
		this.dataManager.set(WARRIOR_TYPE, type);
	}

	public int getWarriorType()
	{
		return this.dataManager.get(WARRIOR_TYPE);
	}

	public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor)
	{
		ItemStack itemstack = new ItemStack(NetherItems.netherrack_arrow);
		AbstractArrowEntity abstractarrowentity = ProjectileHelper.func_221272_a(this, itemstack, distanceFactor);
		if (this.getHeldItemMainhand().getItem() instanceof net.minecraft.item.BowItem)
			abstractarrowentity = ((net.minecraft.item.BowItem) this.getHeldItemMainhand().getItem()).customeArrow(abstractarrowentity);
		double d0 = target.posX - this.posX;
		double d1 = target.getBoundingBox().minY + (double) (target.getHeight() / 3.0F) - abstractarrowentity.posY;
		double d2 = target.posZ - this.posZ;
		double d3 = (double) MathHelper.sqrt(d0 * d0 + d2 * d2);
		abstractarrowentity.shoot(d0, d1 + d3 * (double) 0.2F, d2, 1.6F, (float) (14 - this.world.getDifficulty().getId() * 4));
		this.playSound(SoundEvents.ENTITY_ARROW_SHOOT, 1.0F, 1.0F / (this.getRNG().nextFloat() * 0.4F + 0.8F));
		this.world.addEntity(abstractarrowentity);
	}
}
