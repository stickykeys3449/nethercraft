package com.legacy.nethercraft.entity.tribal;

import javax.annotation.Nullable;

import com.legacy.nethercraft.entity.BloodyZombieEntity;
import com.legacy.nethercraft.entity.DarkZombieEntity;
import com.legacy.nethercraft.entity.CamouflageSpiderEntity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.entity.monster.ZombiePigmanEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;

public class TribalEntity extends MonsterEntity
{

	public TribalEntity(EntityType<? extends TribalEntity> type, World world)
	{
		super(type, world);
		this.setPathPriority(PathNodeType.LAVA, 8.0F);
	}

	protected void registerGoals()
	{
		this.goalSelector.addGoal(7, new WaterAvoidingRandomWalkingGoal(this, 1.0D));
		this.targetSelector.addGoal(1, new TribalEntity.HurtByAggressorGoal(this));
		this.goalSelector.addGoal(8, new LookAtGoal(this, PlayerEntity.class, 8.0F));
		this.goalSelector.addGoal(8, new LookRandomlyGoal(this));
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<ZombiePigmanEntity>(this, ZombiePigmanEntity.class, true));
		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<DarkZombieEntity>(this, DarkZombieEntity.class, true));
		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<BloodyZombieEntity>(this, BloodyZombieEntity.class, true));
		this.targetSelector.addGoal(4, new NearestAttackableTargetGoal<CamouflageSpiderEntity>(this, CamouflageSpiderEntity.class, true));
	}

	protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(25.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue((double) 0.30F);
	}

	@Nullable
	public ILivingEntityData onInitialSpawn(IWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag)
	{
		spawnDataIn = super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
		this.setEquipmentBasedOnDifficulty(difficultyIn);
		this.setEnchantmentBasedOnDifficulty(difficultyIn);
		return spawnDataIn;
	}

	public boolean isNotColliding(IWorldReader worldIn)
	{
		return worldIn.checkNoEntityCollision(this) && !worldIn.containsAnyLiquid(this.getBoundingBox());
	}

	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
	}

	public void readAdditional(CompoundNBT compound)
	{
		super.readAdditional(compound);
	}

	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else
		{
			return super.attackEntityFrom(source, amount);
		}
	}

	protected SoundEvent getAmbientSound()
	{
		this.playSound(SoundEvents.ENTITY_ZOMBIE_PIGMAN_AMBIENT, this.getSoundVolume(), this.getSoundPitch() - 0.3F);
		return null;
	}

	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return SoundEvents.ENTITY_GENERIC_HURT;
	}

	protected SoundEvent getDeathSound()
	{
		return SoundEvents.ENTITY_GENERIC_BIG_FALL;
	}

	protected void setEquipmentBasedOnDifficulty(DifficultyInstance difficulty)
	{
	}

	protected ItemStack getSkullDrop()
	{
		return ItemStack.EMPTY;
	}

	public boolean isPreventingPlayerRest(PlayerEntity playerIn)
	{
		return false;
	}

	@Override
	public boolean isOnSameTeam(Entity entityIn)
	{
		if (entityIn == null)
		{
			return false;
		}
		else if (entityIn == this)
		{
			return true;
		}
		else if (super.isOnSameTeam(entityIn))
		{
			return true;
		}
		else if (entityIn instanceof TribalEntity)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	static class HurtByAggressorGoal extends HurtByTargetGoal
	{

		public HurtByAggressorGoal(TribalEntity p_i45828_1_)
		{
			super(p_i45828_1_);
			this.setCallsForHelp(new Class[]
			{ ZombieEntity.class });
		}

		protected void setAttackTarget(MobEntity mobIn, LivingEntity targetIn)
		{
			if (mobIn instanceof TribalEntity && this.goalOwner.canEntityBeSeen(targetIn))
			{
				mobIn.setAttackTarget(targetIn);
			}
		}
	}
}